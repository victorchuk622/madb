//
//  cellAttr.h
//  ccapp
//
//  Created by YUKI.N on 13年4月11日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cellAttr : NSObject

@property (weak, nonatomic) NSString *CellImage;

@property (weak, nonatomic) NSString *CellLabel;

@property (weak, nonatomic) NSString *CellIdef;

- (id)initWithImage:Image andLabel:Label;

@end
