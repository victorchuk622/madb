//
//  ccappEEJoinEventVC.h
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "listEvent.h"

@interface ccappEEJoinEventVC : UITableViewController

@property NSArray *listEvent;

- (IBAction)F5act:(id)sender;

- (void) loadReload ;

@end
