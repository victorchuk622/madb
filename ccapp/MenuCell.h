//
//  MenuCell.h
//  ccapp
//
//  Created by YUKI.N on 13年4月3日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface menuCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *MenuImage;

@property (weak, nonatomic) IBOutlet UILabel *MenuLabel;

@property (weak, nonatomic) IBOutlet UIButton *MenuBtn;


@end
