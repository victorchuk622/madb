//
//  ccappEEHostEventVC.m
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import "ccappEEHostEventVC.h"

#import "ccappEEHostDetailVC.h"

@interface ccappEEHostEventVC ()

@property NSArray *RestaurantArray;

@end

@implementation ccappEEHostEventVC


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSArray *Amoy = [[NSArray alloc] initWithObjects:@"居食屋「和民」", nil];
    NSArray *KEC = [[NSArray alloc] initWithObjects:@"譚仔雲南米線", nil];
    self.RestaurantArray = [[NSMutableArray alloc] initWithObjects:Amoy, KEC, nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.RestaurantArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [[self.RestaurantArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // static NSString *CellIdentifier = @"Cell";
    // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    //  return cell;
    
    
    
    //製作可重復利用的表格欄位Cell
    static NSString *CellIdentifier = @"RestaurantItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //設定欄位的內容與類型
    cell.textLabel.text = [[self.RestaurantArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return cell;
    
    
    
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"淘大花園";
            break;
            
        case 1:
            return @"KEC附近";
            break;
            
        default:
            return @"";
            break;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    NSString *RestaurantName = [[self.RestaurantArray objectAtIndex:path.section] objectAtIndex:path.row];
    
    ccappEEHostDetailVC *dvController = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantDetail"];
    
    //initWithNibName:@"DetailView" bundle:[NSBundle mainBundle]];
    dvController.RestaurantName = RestaurantName;
    NSInteger RestaurantCode = (int)path.section*10 + (int)path.row;
    
    //NSLog(@"%i",RestaurantCode);
    dvController.RestaurantCode = RestaurantCode;
    [self.navigationController pushViewController:dvController animated:YES];
    dvController = nil;
    
}
@end
