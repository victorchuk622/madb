//
//  ccappMapDispVC.m
//  ccapp
//
//  Created by YUKI.N on 13年4月12日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "ccappMapDispVC.h"

@interface ccappMapDispVC ()

@end

@implementation ccappMapDispVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewKEC
{
    CLLocationCoordinate2D KEC_Corr;
    CLLocationDistance visibleDistance = 0.6*1609.344;
    
    KEC_Corr.latitude = 22.32440;
    KEC_Corr.longitude = 114.21093;
    MKPointAnnotation *KEC = [[MKPointAnnotation alloc]init];
    KEC.coordinate = KEC_Corr;
    KEC.title = @"HKUSPACE KEC Campus";
    KEC.subtitle = @"28 Wang Hoi Rd";
    
    MKCoordinateRegion KECregion = MKCoordinateRegionMakeWithDistance(KEC_Corr, visibleDistance, visibleDistance);
    
    //[self.campusView setRegion:[self.campusView regionThatFits:KECregion] animated:NO];
    //[self.campusView setCenterCoordinate:KEC_Corr  animated:NO];
    
    [self.campusView setRegion:KECregion animated:YES];
    [self.campusView addAnnotation:KEC];
    [self.campusView selectAnnotation:KEC animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
       
    
    
    self.campusView.delegate = self;
        
    [self viewKEC];
    
    [self viewKEC];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mvRefresh:(id)sender {
    
    [self viewKEC];
}
@end
