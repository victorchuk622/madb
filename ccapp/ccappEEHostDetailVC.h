//
//  ccappEEHostDetailVC.h
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ccappEEHostDetailVC : UITableViewController

@property NSString *RestaurantName;

@property NSInteger RestaurantCode;

@property NSDictionary *RestaurantDetailDict;

@property NSString *hostId;

@property (strong, nonatomic) IBOutlet UIDatePicker *DatePicker;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *hostAct;

@property NSDate *selectedDate;

-(void) viewEvent;

@end
