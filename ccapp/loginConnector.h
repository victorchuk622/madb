//
//  loginConnector.h
//  ccapp
//
//  Created by YUKI.N on 13年4月4日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CommonCrypto/CommonDigest.h>

#define SERVER @"http://soruly.sytes.net:8080/"

#define LOGIN @"login?"

#define PREFIX @"I2@3Fj0$f2"

@interface loginConnector : NSObject

@property Boolean loginStatus;

@property NSString *loginSID;

@property NSString *loginPWD;

@property NSString *session;

@property NSUserDefaults *loginInfo;

@property NSString *name;

+ (loginConnector *)sharedLoginConnector;

- (void) load;

- (int) makeConnect;

- (BOOL) disconnect;

- (int) login;

- (id) jsonParser:(NSData*)jsonData;

@end
