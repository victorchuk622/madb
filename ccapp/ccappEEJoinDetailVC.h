//
//  ccappJoinDetailVC.h
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ccappEEJoinDetailVC : UITableViewController

@property NSString *eventID;

@property NSDate *eventDate;

@property NSString *eventRestaurant;

@property NSString *eventHost;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *JoinBtn;

- (IBAction)JoinAct:(id)sender;

@end
