//
//  ccappSOULVC.m
//  gp4_madA
//
//  Created by YUKI.N on 13年4月19日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import "ccappSOULVC.h"

@interface ccappSOULVC ()

@end

@implementation ccappSOULVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSURL *url = [NSURL URLWithString:@"https://soul.hkuspace.hku.hk/"];
    
    
    
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    [self.soulWV loadRequest:urlReq];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
