//
//  ccappHomeVC.h
//  ccapp
//
//  Created by YUKI.N on 13年4月3日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "loginConnector.h"


@interface ccappHomeVC : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
UIScrollViewDelegate,
UIAlertViewDelegate>

@property (nonatomic, retain) NSArray *arrayIcon;

@property (nonatomic, retain) NSArray *arrayMenuLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *mainMenu;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginBtn;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingBtn;

@property (strong, nonatomic) IBOutlet UIPageControl *homePgCtr;



- (IBAction)loginAction:(id)sender;

- (IBAction)menuBtnAction:(id)sender;

- (IBAction)homePgCtrChanged:(id)sender;

- (UIImage *)convertImageToGrayScale:(UIImage *)image;


@end
