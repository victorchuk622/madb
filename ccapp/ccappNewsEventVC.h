//
//  ccappNewsVC.h
//  ccapp
//
//  Created by YUKI.N on 13年4月5日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ccappNewsEventVC : UIViewController <UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *newsWV;

@end
