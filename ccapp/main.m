//
//  main.m
//  ccapp
//
//  Created by YUKI.N on 13年4月3日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ccappAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ccappAppDelegate class]));
    }
}
