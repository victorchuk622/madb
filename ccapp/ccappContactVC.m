//
//  ccappContactVC.m
//  gp3_madA
//
//  Created by YUKI.N on 13年4月19日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import "ccappContactVC.h"

@interface ccappContactVC ()

@end

@implementation ccappContactVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    super.tabBarController.title = @"Contact";
    NSURL *url = [NSURL URLWithString:@"http://hkuspace.hku.hk/about-us/contact-us"];
    
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    [self.contWV loadRequest:urlReq];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
