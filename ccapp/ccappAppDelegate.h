//
//  ccappAppDelegate.h
//  ccapp
//
//  Created by YUKI.N on 13年4月3日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ccappAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
