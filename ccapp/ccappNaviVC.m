//
//  ccappNaviVC.m
//  ccapp
//
//  Created by YUKI.N on 13年4月4日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "ccappNaviVC.h"

@interface ccappNaviVC ()

@end

@implementation ccappNaviVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
