//
//  ccappSettingVC.h
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ccappSettingVC : UITableViewController

@property (strong, nonatomic) IBOutlet UISwitch *autoLoginSwt;

@property (strong, nonatomic) IBOutlet UISwitch *moodleSwt;

@property (strong, nonatomic) IBOutlet UISwitch *chuSwt;

- (IBAction)autoLoginAct:(id)sender;

- (IBAction)moodleAct:(id)sender;

- (IBAction)chuAct:(id)sender;


@end
