//
//  ccappHomeVC.m
//  ccapp
//
//  Created by YUKI.N on 13年4月3日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "ccappHomeVC.h"
#import "menuCell.h"

@interface ccappHomeVC ()

{
    
    BOOL loginStatus;
}

@property NSUserDefaults *userDefault;

@property (atomic) loginConnector *HomeLoginConnector;

@property AVAudioPlayer *audioPlayer;

@property UIColor *originalLoginBtnColor;



@end

@implementation ccappHomeVC

- (void)viewDidLoad


{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background.png"]];
    

    self.userDefault = [NSUserDefaults standardUserDefaults];
    if(nil == [self.userDefault objectForKey:@"initialized_defaults"])
    {
        
        [self.userDefault setBool:YES forKey:@"autoLogin"];
        [self.userDefault setBool:YES forKey:@"moodle"];
        [self.userDefault setBool:YES forKey:@"chu"];
        
        [self.userDefault setObject:@"dummy" forKey:@"initialized_defaults"];
        
        [self.userDefault synchronize];
    }
    
    [self dirtySetBtnImg];   
    
    self.HomeLoginConnector = [loginConnector sharedLoginConnector];    
    
    self.originalLoginBtnColor = _loginBtn.tintColor;
    
    [self loginStatusChk];
    
    
    //page ctrl
    UICollectionViewFlowLayout *flowLayout = [
                                              [UICollectionViewFlowLayout alloc] init
                                              ];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    
    [[self mainMenu]setDataSource:self];
    [[self mainMenu]setDelegate:self];
    
    
    
    [self prepareData];
}

//  navi icon scale dirty fix, bug! I hate Apple!
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self dirtySetBtnImg];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [self dirtySetBtnImg];
    [self prepareData];
    [self.mainMenu reloadData];
}


- (void)dirtySetBtnImg
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        self.settingBtn.imageInsets = UIEdgeInsetsMake(4, 12, 4, 12);
    }
    else
    {
        self.settingBtn.imageInsets = UIEdgeInsetsMake(5, 10, 5, 10);
    }
}

//- end navi icon fix

//  datasource and delegate method - HomeCollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;/*
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        return 1;
    }
    else
    {
        return 2;
    }*/
}

- (NSInteger)collectionView: (UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_arrayMenuLabel count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
    menuCell *MenuCell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [[MenuCell MenuImage]setImage:[UIImage imageNamed:[_arrayIcon objectAtIndex:indexPath.item]]];
    [[MenuCell MenuLabel]setText:[_arrayMenuLabel objectAtIndex:indexPath.item]];
    
    
    if([MenuCell.MenuLabel.text length] >= 15)
    {
        MenuCell.MenuLabel.font = [MenuCell.MenuLabel.font fontWithSize:10];
    }
    [[MenuCell MenuBtn]setTag:indexPath.item];
    
    
    
    if(!self.HomeLoginConnector.loginStatus)
    {
        if(MenuCell.MenuBtn.tag == 4 || MenuCell.MenuBtn.tag == 8 || MenuCell.MenuBtn.tag == 9)
            [[MenuCell MenuImage]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@%@",[_arrayIcon objectAtIndex:indexPath.item],@"-grey"]]];
    }
    
    
    //[[MenuCell MenuLabel]setTextColor:[UIColor whiteColor]];
    return MenuCell;
}

- (IBAction)loginAction:(id)sender
{
    if(![self.HomeLoginConnector loginStatus])
    {
        [self showLoginDialog];
    }
    else
    {
        [self showLogoutDialog];
    }
}

//handle button action
- (IBAction)menuBtnAction:(id)sender {
   
    [self playChu];
    
    UIButton *btn = sender;
    
    switch(btn.tag)
    {
        case 0:
            [self performSegueWithIdentifier:@"HomeToNews" sender:self];
            break;            
        case 1:
            [self performSegueWithIdentifier:@"HomeToCal" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"HomeToActivities" sender:self];
            break;
        case 3:
            [self performSegueWithIdentifier:@"HomeToExam" sender:self];
            break;
        case 4:
            if (!self.HomeLoginConnector.loginStatus)
                break;
            else
            {
                [self performSegueWithIdentifier:@"HomeToSoul" sender:self];
                break;
            }
        case 5:
            [self performSegueWithIdentifier:@"HomeToMap" sender:self];
            break;
        case 6:
            [self performSegueWithIdentifier:@"HomeToContact" sender:self];
            break;
        case 7:
            [self performSegueWithIdentifier:@"HomeToTips" sender:self];
            break;
        case 8:
            /*
             if (![self.HomeLoginConnector loginStatus])
             {
             [self showNotYetLogin];
             }
             else
             {
             [self showShit];
             //[self performSegueWithIdentifier:@"HomeToMail" sender:self];
             }
             */
            if (!self.HomeLoginConnector.loginStatus)
                break;
            else
            {
                [self performSegueWithIdentifier:@"HomeToMail" sender:self];
                break;
            }
            break;
        case 9:
            if (!self.HomeLoginConnector.loginStatus)
                break;
            else
            {
                [self performSegueWithIdentifier:@"HomeToEat" sender:self];
                break;
            }
            break;
        default:
            [self showShit];
            break;
    }
    /*
     NSString *testMsg = [NSString stringWithFormat:@"Button Pressed:%d",btn.tag];
     UIAlertView *btnTest = [[UIAlertView alloc] initWithTitle:@"Button Pressed" message:testMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [btnTest show];
     */
    
}
// page ctrl
- (IBAction)homePgCtrChanged:(id)sender
{
    
    UIPageControl *pageControl = sender;
    CGFloat pageWidth = self.mainMenu.frame.size.width;
    CGPoint scrollTo = CGPointMake(pageWidth * pageControl.currentPage, 0);
    [self.mainMenu setContentOffset:scrollTo animated:YES];
}


//handle login action (in real implement self->loginStatus should be connector status

- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self playChu];
    
    if(alertView.tag == 0 && buttonIndex == 0)
    {
        [self.HomeLoginConnector setLoginSID:[[alertView textFieldAtIndex:0] text]];
                      
        [self.HomeLoginConnector setLoginPWD:[[alertView textFieldAtIndex:1] text]];
        
        //[self.HomeLoginConnector loginConnect];
        [self login];
        [self loginBtnCtrl];
        [self.mainMenu reloadData];
    }
    
    if(alertView.tag == 1 && buttonIndex == 0)
    {
        [self.HomeLoginConnector disconnect];
        [self loginBtnCtrl];
        [self.mainMenu reloadData];

        /*
        if(![self.HomeLoginConnector loginStatus])
        {
            
            _loginBtn.tintColor = nil;
            _loginBtn.title = @"Login";
        }
        
        else
        {
            [self showFailDialog];
        }
         */
    }
}

//login dialog
- (void) showLoginDialog;
{
    UIAlertView *loginDiag = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Enter your SID and Password" delegate:self cancelButtonTitle:@"Login" otherButtonTitles:@"Cancel", nil];
    
    loginDiag.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [loginDiag setTag:0];
    
    UITextField *SID = [loginDiag textFieldAtIndex:0];
    SID.placeholder = @"Enter your SID";
    SID.clearButtonMode = UITextFieldViewModeWhileEditing;
    SID.keyboardType = UIKeyboardTypeEmailAddress;
    SID.keyboardAppearance = UIKeyboardAppearanceAlert;
    SID.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    UITextField *Password = [loginDiag textFieldAtIndex:1];
    Password.placeholder = @"Enter your Password";
    Password.clearButtonMode = UITextFieldViewModeWhileEditing;
    Password.keyboardType = UIKeyboardTypeEmailAddress;
    Password.keyboardAppearance = UIKeyboardAppearanceAlert;
    Password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    Password.autocapitalizationType = UITextAutocorrectionTypeNo;
    
    [loginDiag show];
}
//success dialog
- (void) showSuccessDialog
{
    UIAlertView *SuccessDiag = [[UIAlertView alloc] initWithTitle:@"Login Success" message:@"Press OK to return." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
    [SuccessDiag show];
}
//fail dialog
- (void) showFailDialog
{
    UIAlertView *FailDiag = [[UIAlertView alloc] initWithTitle:@"Login Fail" message:@"Wrong SID or Password!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
    [FailDiag show];
}
//logout dialog
- (void) showLogoutDialog
{
    UIAlertView *LogoutDiag = [[UIAlertView alloc] initWithTitle:@"Logout" message:@"Confirm Logout?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No" , nil];
    [LogoutDiag setTag:1];
    [LogoutDiag show];
}
//
//logout dialog
- (void) showNotYetLogin
{
    UIAlertView *NotYetLoginDiag = [[UIAlertView alloc] initWithTitle:@"Please Login" message:@"You haven't Login!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
    [NotYetLoginDiag setTag:99];
    [NotYetLoginDiag show];
}
//no connection dialog
- (void) showNoConnection
{
    UIAlertView *NoConnectionDiag = [[UIAlertView alloc] initWithTitle:@"Login Fail" message:@"Check your Internet Connection!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
    [NoConnectionDiag setTag:90];
    [NoConnectionDiag show];
}
//shit dialog
- (void) showShit
{
    UIAlertView *Shit = [[UIAlertView alloc] initWithTitle:@"Houston, we have a problem!" message:@"Developing!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
    [Shit show];
    //exit(0);
}
//text resize
- (void) fontResize :(UILabel*) label
{
    if([label.text length] >= 15)
    {
        [label.font fontWithSize:12];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//page ctrl again
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.mainMenu.frame.size.width;
    self.homePgCtr.currentPage = self.mainMenu.contentOffset.x / pageWidth;
}

//loginfunction
- (void)login
{
    switch([self.HomeLoginConnector makeConnect])
    {
        case 0:
            [self showSuccessDialog];
            break;
        case 1:
            [self showFailDialog];
            break;
        case 2:
            [self showNoConnection];
            break;
    }
}
- (void)loginBtnCtrl
{    
    if([self.HomeLoginConnector loginStatus])
    {
        _loginBtn.tintColor = [UIColor redColor];
        _loginBtn.title = @"Logout";
    }    
    else
    {
        
        _loginBtn.tintColor = self.originalLoginBtnColor;
        _loginBtn.title = @"Login";
    }
}
- (void)loginStatusChk
{
    if([self.HomeLoginConnector loginStatus])
    {
        [self login];
    }
    [self loginBtnCtrl];
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image
{
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    /* changes start here */
    // Create bitmap image info from pixel data in current context
    CGImageRef grayImage = CGBitmapContextCreateImage(context);
    // release the colorspace and graphics context
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    // make a new alpha-only graphics context
    context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, nil, kCGImageAlphaOnly);
    // draw image into context with no colorspace
    CGContextDrawImage(context, imageRect, [image CGImage]);
    // create alpha bitmap mask from current context
    CGImageRef mask = CGBitmapContextCreateImage(context);
    // release graphics context
    CGContextRelease(context);
    // make UIImage from grayscale image with alpha mask
    UIImage *grayScaleImage = [UIImage imageWithCGImage:CGImageCreateWithMask(grayImage, mask) scale:image.scale orientation:image.imageOrientation];
    // release the CG images
    CGImageRelease(grayImage);
    CGImageRelease(mask);
    // return the new grayscale image
    return grayScaleImage;
    /* changes end here */
}

- (void) playChu
{
    if([self.userDefault boolForKey:@"chu"])
    {
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/chu.wav",[[NSBundle mainBundle]resourcePath]]];
        
        NSError *error;
        
        self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
        
        self.audioPlayer.numberOfLoops = 0;
        
        
        [self.audioPlayer play];   
    
    }
    
}

- (void)prepareData
{
    //end page ctrl
    if([self.userDefault boolForKey:@"moodle"])
        _arrayIcon = [[NSArray alloc ]initWithObjects:
                      @"News",
                      @"Calendar",
                      
                      @"Activities",
                      @"Exam",
                      @"SOUL-m",
                      
                      @"Map",
                      @"Contact",
                      @"Tips",
                      @"Mail",
                      @"Food",
                      nil];
    else
        _arrayIcon = [[NSArray alloc ]initWithObjects:
                      @"News",
                      @"Calendar",
                      
                      @"Activities",
                      @"Exam",
                      @"SOUL-e",
                      
                      @"Map",
                      @"Contact",
                      @"Tips",
                      @"Mail",
                      @"Food",
                      nil];
    
    
    
    
    _arrayMenuLabel = [[NSArray alloc]initWithObjects:
                       @"News & Event",
                       @"Calendar",
                       
                       @"Activities",
                       @"Exam",
                       @"SOUL",
                       
                       @"Map",
                       @"Contact & Facilities",
                       @"Tips",
                       @"Portal Mail",
                       @"EatEasy!",
                       nil];
}



@end
