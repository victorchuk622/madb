//
//  ccappJoinDetailVC.m
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import "ccappEEJoinDetailVC.h"

#import "ccappEEJoinEventVC.h"

#import "loginConnector.h"

@interface ccappEEJoinDetailVC ()

@property NSMutableArray *showDetail;

@property NSMutableArray *checkHost;

@property loginConnector *jsonConnector;

- (void)loadData;

@end

@implementation ccappEEJoinDetailVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.eventRestaurant;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.jsonConnector = [loginConnector sharedLoginConnector];
    
    [self loadData];
    
    for(NSDictionary *target in self.checkHost)
    {
    
    if ([[target objectForKey:@"name"]isEqualToString:self.jsonConnector.name])
    {
        [self.JoinBtn setEnabled:NO];
    }
    }
    
   // NSLog(@"%@",self.eventID);
}



- (void)loadData
{
    NSString * showDetailStr = [NSString stringWithFormat:@"%@show?id=%@&key=%@&meal=%@",SERVER,self.jsonConnector.loginSID,self.jsonConnector.session,self.eventID];
    
    self.showDetail = [NSMutableArray arrayWithArray:[self.jsonConnector jsonParser:[NSData dataWithContentsOfURL:[NSURL URLWithString:showDetailStr]]]];
    
    self.checkHost = [NSMutableArray arrayWithArray:[self.jsonConnector jsonParser:[NSData dataWithContentsOfURL:[NSURL URLWithString:showDetailStr]]]];
;
    
    [self.showDetail removeObjectAtIndex:0];
    
    //[self.showDetail addObjectsFromArray:Detail];
    
    //[self.showDetail removeObjectAtIndex:0];
    
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    switch(section)
    {
        case 0:
            return 1;
            break;
        case 1:
            return 1;
            break;
        case 2:
            
            if ([self.showDetail count] == 0)
            {
                return 1;
            }
            return [self.showDetail count];
            break;      
            
        default:
            return 0;
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Meet at:";
            break;
            
        case 1:
            return @"Host";
            break;
            
        case 2:
            return @"Member";
            break;
        default:
            return @"";
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    @try{
    NSDateFormatter *sqlFormatter = [[NSDateFormatter alloc] init];
        NSLocale *usLocale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        [sqlFormatter setLocale:usLocale];
    [sqlFormatter setDateFormat:@"u MMM d h:mmaa"];
    
    if(indexPath.section == 0)
    {
        cell.textLabel.text = [sqlFormatter stringFromDate:self.eventDate];
        
        //NSLog(@"%@",[sqlFormatter stringFromDate:self.eventDate]);
        return cell;
    }
    
    if(indexPath.section == 1)
    {
        cell.textLabel.text = self.eventHost;
        return cell;
    }
    
    if(indexPath.section == 2)
    {
        //cell.textLabel.text = [[self.showDetail objectAtIndex:indexPath.row]objectForKey:@"name"];
        @try {
            cell.textLabel.text = [[self.showDetail objectAtIndex:indexPath.row]objectForKey:@"name"];
        }
        @catch (NSException *exception) {
            if(self.JoinBtn.isEnabled)
                cell.textLabel.text = @"No member, join now!";
            else
                cell.textLabel.text = @"No member";
            return cell;
        }
        return cell;
    }
    return cell;
    }
    @catch (NSException *exception) {
        UIAlertView *FailDiag = [[UIAlertView alloc] initWithTitle:@"Connection Fail" message:@"Please check your network connection or try again!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
        [FailDiag show];
        [self.jsonConnector login];
        return cell;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (IBAction)JoinAct:(id)sender {
    
    NSString * joinStr = [NSString stringWithFormat:@"%@join?id=%@&key=%@&meal=%@",SERVER,self.jsonConnector.loginSID,self.jsonConnector.session,self.eventID];
    
    NSDictionary *joinResult = [self.jsonConnector jsonParser:[NSData dataWithContentsOfURL:[NSURL URLWithString:joinStr]]];
    
    NSString *joinId = [joinResult objectForKey:@"id"];
    
    //ccappEEJoinEventVC *dvController = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinEvent"];
    
    //[dvController loadReload];
    
    //[ccappEEJoinEventVC loadReload];
    
    if ([joinId length] != 0)
    {
        UIAlertView *SuccessDiag = [[UIAlertView alloc] initWithTitle:@"Join Success" message:@"Press OK to return." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
        [self loadData];
        [self.tableView reloadData];
        [SuccessDiag show];
        
    }
    else
    {
        UIAlertView *FailDiag = [[UIAlertView alloc] initWithTitle:@"Join Fail" message:@"Please check your network connection!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
        [FailDiag show];
    }

    
    
}
@end
