//
//  ccappMapDispVC.h
//  ccapp
//
//  Created by YUKI.N on 13年4月12日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>




@interface ccappMapDispVC : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *campusView;

- (IBAction)mvRefresh:(id)sender;
@end
