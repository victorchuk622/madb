//
//  KECAnno.h
//  ccapp
//
//  Created by YUKI.N on 13年4月12日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface KECAnno : NSObject <MKAnnotation>

@end
