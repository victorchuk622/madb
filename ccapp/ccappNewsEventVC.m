//
//  ccappNewsVC.m
//  ccapp
//
//  Created by YUKI.N on 13年4月5日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "ccappNewsEventVC.h"

@interface ccappNewsEventVC ()

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@property (weak, nonatomic) UIViewController *subViewController;

@end

@implementation ccappNewsEventVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tabBar.delegate = self;
    self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
   
    [self openNewsWeb];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    //[self performSegueWithIdentifier:@"NEToEvent" sender:self];
    //NSLog(@"btn pressed");
    if (item.tag == 0)
    {
        [self openNewsWeb];
        if ([self.view viewWithTag:1]!=nil)
        {
            //[[self.view viewWithTag:1] removeFromSuperview];
        }
    }
    if (item.tag == 1)
    {
        
        [self openEventWeb];
    }
}

- (void)openNewsWeb
{
    /*
     NSURL *url = [NSURL URLWithString:@"https://learner1.hkuspace.hku.hk/lppriv/weblog/ShowArticleGroup.php"];
     */
    
    /*
     NSURL *url = [NSURL URLWithString:@"https://learner1.hkuspace.hku.hk/lpmedia/copy/channel/20.html"];
     */
    NSURL *url = [NSURL URLWithString:@"http://hkuspace.hku.hk/news"];
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    [self.newsWV loadRequest:urlReq];
    self.title = @"News";
}

- (void)openEventWeb
{
    /*
     self.subViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabEvent"];
     
     [self.view insertSubview:self.subViewController.view belowSubview:self.tabBar];
     */
    
    
    NSURL *url = [NSURL URLWithString:@"http://hkuspace.hku.hk/event"];
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    [self.newsWV loadRequest:urlReq];
    self.title = @"Event";
}

@end
