//
//  ccappEEJoinEventVC.m
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import "ccappEEJoinEventVC.h"

#import "ccappEEJoinDetailVC.h"

#import "loginConnector.h"

@interface ccappEEJoinEventVC ()

@property loginConnector *jsonConnector;

- (void)loadData;

@end

@implementation ccappEEJoinEventVC


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadData
{
    @try {
        NSString * listStr = [NSString stringWithFormat:@"%@list?id=%@&key=%@",SERVER,self.jsonConnector.loginSID,self.jsonConnector.session];
        
        self.ListEvent = [self.jsonConnector jsonParser:[NSData dataWithContentsOfURL:[NSURL URLWithString:listStr]]];
    }
    @catch (NSException *exception) {
        //do nth
    
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.jsonConnector = [loginConnector sharedLoginConnector];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    [self loadData];
}



- (void)viewWillAppear:(BOOL)animated
{
    [self loadReload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.listEvent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"listEvent";
    listEvent *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    @try {
        switch([[[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"restaurant"] intValue])
    {
        case 0:
            cell.restaurant.text = @"居食屋「和民」";
            break;
        case 10:
            cell.restaurant.text = @"譚仔雲南米線";
            break;
    };
    
    if ([[[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"joined" ] boolValue]) {
        //cell.backgroundColor = [UIColor greenColor];
        
        UIView* bgview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        bgview.opaque = YES;
        bgview.backgroundColor = [UIColor colorWithRed:0.503222  green:0.953986  blue:0.503222 alpha:0.5];
        [cell setBackgroundView:bgview];
        
        cell.subtitle.textColor = [UIColor orangeColor];
    }
    
    NSDateFormatter *sqlFormatter = [[NSDateFormatter alloc] init];
    [sqlFormatter setDateFormat:@"u-MM-dd HH:mm:00"];
    NSDate *eventDate = [sqlFormatter dateFromString:[[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"time"]];
    
    NSDateFormatter *simDipFormatter = [[NSDateFormatter alloc] init];
        NSLocale *usLocale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        [simDipFormatter setLocale:usLocale];
        [simDipFormatter setDateFormat:@"M/d h:mmaa"];
    
    cell.rightDetail.text = [simDipFormatter stringFromDate:eventDate];
    cell.subtitle.text = [NSString stringWithFormat:@"%@ People joined",[[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"num"]];
    
    return cell;
    }
    @catch (NSException *exception) {
        UIAlertView *FailDiag = [[UIAlertView alloc] initWithTitle:@"Connection Fail" message:@"Please check your network connection or try refresh!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
        [FailDiag show];
        [self.jsonConnector login];
        return cell;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)loadReload {
    [self loadData];
    [self.tableView reloadData];
}

- (IBAction)F5act:(id)sender {
    
    [self loadReload];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    
    ccappEEJoinDetailVC *dvController = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinDetail"];
    
    //initWithNibName:@"DetailView" bundle:[NSBundle mainBundle]];
    
    NSDateFormatter *sqlFormatter = [[NSDateFormatter alloc] init];
    [sqlFormatter setDateFormat:@"u-MM-dd HH:mm:00"];
    
    dvController.eventID = [[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"id"];
    dvController.eventDate = [sqlFormatter dateFromString:[[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"time"]];
    dvController.eventHost = [[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"host"];
    
    switch([[[self.listEvent objectAtIndex:indexPath.row] objectForKey:@"restaurant"] intValue])
    {
        case 0:
            dvController.eventRestaurant = @"居食屋「和民」";
            break;
        case 10:
            dvController.eventRestaurant = @"譚仔雲南米線";
            break;
    };
        
    [self.navigationController pushViewController:dvController animated:YES];
    dvController = nil;
    
}

@end
