//
//  ccappEEHostDetailVC.m
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import "ccappEEHostDetailVC.h"

#import "ccappEEJoinDetailVC.h"

#import "loginConnector.h"

@interface ccappEEHostDetailVC ()

@property loginConnector *jsonConnector;

@end

@implementation ccappEEHostDetailVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSArray *noodle = [[NSArray alloc] initWithObjects:@"noodle.jpg",@"九龍灣宏開道8號其士商業大廈1樓", nil];
    NSArray *japan = [[NSArray alloc] initWithObjects:@"watami.jpg",@"九龍灣牛頭角道77號淘大商場三期2樓172-174號舖", nil];
    
    
    self.RestaurantDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 noodle, @"10",
                                 japan, @"0",
                                 nil];
    
    self.title = self.RestaurantName;
    
    self.DatePicker.minimumDate = [NSDate date];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [[self.RestaurantDetailDict objectForKey:[NSString stringWithFormat:@"%d", self.RestaurantCode]] count]+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    switch (section) {
        case 2:
            return 0;
            break;
            
            
        default:
            return 1;
            break;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*static NSString *CellIdentifier = @"Cell";
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     
     // Configure the cell...
     
     return cell;
     */
    static NSString *CellIdentifier = @"RestaurantDetailItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
    }
    
    //設定欄位的內容與類型
    if(indexPath.row == 0 && indexPath.section == 0)
    {
        cell.imageView.image = [UIImage imageNamed:[[self.RestaurantDetailDict objectForKey:[NSString stringWithFormat:@"%d", self.RestaurantCode]]objectAtIndex:indexPath.section]];
        
        cell.imageView.center = CGPointMake (cell.frame.size.width/2,cell.frame.size.height/2);
        
        
        /*
         UIImageView *pic = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[[self.RestaurantDetailDict objectForKey:[NSString stringWithFormat:@"%d", self.RestaurantCode]]objectAtIndex:indexPath.section]]];
         pic.center = CGPointMake(cell.contentView.bounds.size.width / 2 , 60);
         pic.contentMode = UIViewContentModeScaleAspectFit;
         cell.selectionStyle = UITableViewCellSelectionStyleGray;
         
         [cell.contentView addSubview:pic];*/
        
    }
    else{
        cell.textLabel.text = [[self.RestaurantDetailDict objectForKey:[NSString stringWithFormat:@"%d", self.RestaurantCode]]objectAtIndex:indexPath.section] ;
        
    }
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return cell;
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    int height = cell.frame.size.height ;
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 200;
    }
    else
        return height;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return nil;
            break;
            
        case 1:
            return @"Address";
            break;
            
        case 2:
            return @"Select Date and Time:";
            break;
        default:
            return @"";
            break;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void) viewEvent
{
    
    
    //NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    
    ccappEEJoinDetailVC *dvController = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinDetail"];
    
    //initWithNibName:@"DetailView" bundle:[NSBundle mainBundle]];
    
    dvController.eventID = self.hostId;
    dvController.eventDate = self.DatePicker.date;
    dvController.eventHost = self.jsonConnector.name;
    dvController.eventRestaurant = self.RestaurantName;
           
    [self.navigationController pushViewController:dvController animated:YES];
    dvController = nil;
    
}

- (IBAction)hostAct:(id)sender {
    
    self.selectedDate = self.DatePicker.date;
    
    NSDateFormatter *dispFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];    
    [dispFormatter setLocale:usLocale];
    [dispFormatter setDateFormat:@"h:mmaa d MMM, u"];
    
    NSString *msg = [NSString stringWithFormat:@"The activity will be held at\r%@, sure?", [dispFormatter stringFromDate:self.selectedDate]];
    
    UIAlertView *ConfirmDiag = [[UIAlertView alloc] initWithTitle:@"Host Event" message:msg delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No" , nil];
    [ConfirmDiag setTag:0];
    [ConfirmDiag show];
}

- (void)showFail
{
    UIAlertView *FailDiag = [[UIAlertView alloc] initWithTitle:@"Host Fail" message:@"Please check your network connection or try refresh!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
    [self.jsonConnector login];
    [FailDiag show];
}

- (void)showSuccess
{
    UIAlertView *SuccessDiag = [[UIAlertView alloc] initWithTitle:@"Host Success" message:@"Press View to view the event." delegate:self cancelButtonTitle:@"View" otherButtonTitles:@"Back",nil , nil];
    [SuccessDiag setTag:1];
    [SuccessDiag show];
}

- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 0 && buttonIndex == 0)
    {
       
        self.jsonConnector = [loginConnector sharedLoginConnector];
        
        NSString *code = [NSString stringWithFormat:@"%i",self.RestaurantCode];
        
        NSDateFormatter *sqlFormatter = [[NSDateFormatter alloc] init];
        [sqlFormatter setDateFormat:@"u-MM-dd%20HH:mm:00"];
        
        NSString * hostStr = [NSString stringWithFormat:@"%@host?id=%@&key=%@&restaurant=%@&time=%@",
                              SERVER,self.jsonConnector.loginSID,self.jsonConnector.session,code,[sqlFormatter stringFromDate:self.selectedDate]];
        
        NSDictionary* hostJson = [self.jsonConnector jsonParser:[NSData dataWithContentsOfURL:[NSURL URLWithString:hostStr]]];
        @try {
            self.hostId = [hostJson objectForKey:@"id"];
            if ([self.hostId length] != 0)
                [self showSuccess];
            else
                [self showFail];
        }
        @catch (NSException *exception) {
            [self showFail];
        }
                
    }
    if(alertView.tag == 1 && buttonIndex == 0)
    {
        
        [self viewEvent];
    }    
}

@end
