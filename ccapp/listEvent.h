//
//  listEventCell.h
//  gp3_madB
//
//  Created by YUKI.N on 13年5月2日.
//  Copyright (c) 2013年 madGP4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface listEvent : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *restaurant;

@property (strong, nonatomic) IBOutlet UILabel *subtitle;

@property (strong, nonatomic) IBOutlet UILabel *rightDetail;




@end
