//
//  ccappCalVC.m
//  ccapp
//
//  Created by YUKI.N on 13年4月5日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "ccappCalVC.h"

@interface ccappCalVC ()

@end

@implementation ccappCalVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Calendar" ofType:@"pdf"];
    
    NSURL *url = [NSURL fileURLWithPath:path];
        
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    [self.calWV loadRequest:urlReq];
    self.calWV.scalesPageToFit = YES;
    self.calWV.autoresizingMask = YES;
    self.calWV.delegate = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
