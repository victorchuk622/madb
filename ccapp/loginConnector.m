//
//  loginConnector.m
//  ccapp
//
//  Created by YUKI.N on 13年4月4日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "loginConnector.h"

@interface loginConnector()

@property NSString *username;

@property NSString *password;



@end

@implementation loginConnector

static loginConnector *_loginConnector = nil;

- (id)init
{
    self = [super init];
    
    [self load];
    
    return self;
}

- (void)load
{
    self.loginInfo = [NSUserDefaults standardUserDefaults];
    self.loginSID = [self.loginInfo stringForKey:@"SID"];
    self.loginPWD = [self.loginInfo stringForKey:@"PWD"];
    /*
     if([self.loginInfo objectForKey:@"status"] == nil)
     {
     self.loginStatus = NO;
     }
     else
     */
    if ([self.loginInfo boolForKey:@"autoLogin"])
        self.loginStatus = [self.loginInfo boolForKey:@"status"];
    else
        self.loginStatus = NO;
}

// login creditental - JSON
- (int) makeConnect
{
    if([self.loginPWD length] == 0)
    {
        self.loginStatus = 0;
        return 1;
    }
        
    self.username = [NSString stringWithFormat:@"id=%@",self.loginSID];
    
    self.password = [NSString stringWithFormat:@"&passwd=%@",[self sha1:[NSString stringWithFormat:@"%@%@",PREFIX,self.loginPWD]]];
    
    return [self login];
}
- (BOOL) disconnect
{
    [self.loginInfo removeObjectForKey:@"SID"];
    [self.loginInfo removeObjectForKey:@"PWD"];
    [self.loginInfo removeObjectForKey:@"status"];
    [self.loginInfo synchronize]; 
    self.loginStatus = NO;
    return YES;
}

- (int)login
{
    NSURL *loginURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@%@",SERVER,LOGIN,self.username,self.password]];
    
    NSDictionary* loginResult = [self jsonParser:[NSData dataWithContentsOfURL:loginURL]];
    
    //check internet connection
    if(loginResult == nil)
    {
        self.loginStatus = NO;
        return 2;
    }
    
    //check return section
    self.session = [loginResult objectForKey:@"key"];
    
    if ([self.session length] != 0)
    {
        self.loginStatus = YES;
        [self.loginInfo setObject:self.loginSID forKey:@"SID"];
        [self.loginInfo setObject:self.loginPWD forKey:@"PWD"];
        [self.loginInfo setBool:self.loginStatus forKey:@"status"];
        self.name = [loginResult objectForKey:@"name"];
        [self.loginInfo synchronize];
        return 0;
    }
    else
    {
        [self clearValue];
        self.loginStatus = NO;
        return 1;
    }
}

// value cleaning
-(void) clearValue
{
    _loginSID = nil;
    _loginPWD = nil;
}










//sha1 - source http://www.makebetterthings.com/iphone/how-to-get-md5-and-sha1-in-objective-c-ios-sdk
-(NSString*) sha1:(NSString*)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(cStr, strlen(cStr), result);
    NSString *s = [NSString  stringWithFormat:
                   @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                   result[0], result[1], result[2], result[3], result[4],
                   result[5], result[6], result[7],
                   result[8], result[9], result[10], result[11], result[12],
                   result[13], result[14], result[15],
                   result[16], result[17], result[18], result[19]
                   ];
    
    return s;
}

//json parser
- (id) jsonParser:(NSData*)jsonData
{
    NSError* error;
    @try {
        return [NSJSONSerialization
                JSONObjectWithData:jsonData
                
                options:kNilOptions
                error:&error];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

//singleton pattern
+ (loginConnector *)sharedLoginConnector {
    
    @synchronized([loginConnector class]) {
        
        //判斷_singletonObject是否完成記憶體配置
        if (!_loginConnector)
            _loginConnector = [[self alloc] init];
        
        return _loginConnector;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([loginConnector class]) {
        
        //避免 [SingletonObject alloc] 方法被濫用
        NSAssert(_loginConnector == nil, @"_loginConector allocted!");
        _loginConnector = [super alloc];
        
        return _loginConnector;
    }
    
    return nil;
}

@end
