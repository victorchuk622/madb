//
//  cellAttr.m
//  ccapp
//
//  Created by YUKI.N on 13年4月11日.
//  Copyright (c) 2013年 Ikaros. All rights reserved.
//

#import "cellAttr.h"

@implementation cellAttr

- (id)initWithImage:(NSString*)Image andLabel:(NSString*)Label
{
    if (self = [super init])
    {
        _CellImage = Image;
        _CellLabel = Label;
        _CellIdef = Label;        
    }
    return (self);
};

@end